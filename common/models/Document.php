<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property int $lesson_id
 * @property string $name
 * @property string $file
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Lesson $lesson
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lesson_id', 'name', 'file', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['lesson_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'file'], 'string', 'max' => 255],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::class, 'targetAttribute' => ['lesson_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lesson_id' => 'Lesson ID',
            'name' => 'Name',
            'file' => 'File',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

	public function getFileUrl(){
		return Yii::getAlias('@web')
			. DIRECTORY_SEPARATOR
			. Yii::$app->params['filesDir']
			. DIRECTORY_SEPARATOR
			. $this->lesson_id
			. DIRECTORY_SEPARATOR
			. $this->file;
	}

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::class, ['id' => 'lesson_id']);
    }
}
