<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Lesson */
/* @var $documents common\models\DocumentSearch */
/* @var $searchDocs yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Lessons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lesson-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<?php if (Yii::$app->user->can('admin')): ?>
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
	<?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
        ],
    ]) ?>

	<?= GridView::widget([
		'dataProvider' => $documents,
		'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'file',
				'format' => 'raw',
				'value' => function($data){
					return
						Html::a($data->file, $data->getFileUrl(), [
//                            'class' => 'btn btn-sm btn-default'
                        ]);
				}
			],

			'name',

		],
	]); ?>

</div>
