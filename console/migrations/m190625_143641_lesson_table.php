<?php

use yii\db\Migration;

/**
 * Class m190625_143641_lesson_table
 */
class m190625_143641_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{lesson}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(255)->notNull(),
			'description' => $this->text()->null(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer()->notNull(),
		]);

		$this->createTable('{{document}}', [
			'id' => $this->primaryKey(),
			'lesson_id' => $this->integer()->notNull(),
			'name' => $this->string(255)->notNull(),
			'file' => $this->string(255)->notNull(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer()->notNull(),
		]);

		$this->addForeignKey(
			'FK_document_lesson_id',
			'{{document}}',
			'lesson_id',
			'{{lesson}}',
			'id',
			'RESTRICT'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey(
			'FK_document_lesson_id',
			'{{document}}'
		);

		$this->dropTable('{{document}}');

		$this->dropTable('{{lesson}}');
    }
}
