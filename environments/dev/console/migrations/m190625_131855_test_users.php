<?php

use yii\db\Migration;

/**
 * Class m190625_131855_test_users
 */
class m190625_131855_test_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$rbac = Yii::$app->authManager;

		$user = $rbac->createRole('user');
		$user->description = 'Users';
		$rbac->add($user);

		$admin = $rbac->createRole('admin');
		$admin->description = 'Administrators';
		$rbac->add($admin);

		$rbac->addChild($admin, $user);

		$this->insert('{{user}}', [
			'id' => 1,
			'username' => 'admin',
			'auth_key' => '',
			'password_hash' => '$2y$13$N.RQ0DHBza3Xq6oRQ0HSM./pZApmgwwc46WBCGyeU0M9gtx/VHhSC',
			'email' => 'admin@site.test',
			'status' => 10,
			'created_at' => time(),
			'updated_at' => time(),
		]);

		$rbac->assign($admin, 1);

		$this->insert('{{user}}', [
			'id' => 2,
			'username' => 'user',
			'auth_key' => '',
			'password_hash' => '$2y$13$N.RQ0DHBza3Xq6oRQ0HSM./pZApmgwwc46WBCGyeU0M9gtx/VHhSC',
			'email' => 'user@site.test',
			'status' => 10,
			'created_at' => time(),
			'updated_at' => time(),
		]);

		$rbac->assign($user, 1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$rbac = Yii::$app->authManager;

		$rbac->removeAll();

		$this->delete('{{user}}');

		$this->execute('ALTER TABLE {{user}} AUTO_INCREMENT = 1;');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190625_131855_test_users cannot be reverted.\n";

        return false;
    }
    */
}
